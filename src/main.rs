// parallelization tools
use rayon::prelude::*;

// concrete
use concrete::*;

fn main() -> Result<(), CryptoAPIError> {

    // encoders
    let encoder_input = Encoder::new(-4., 4., 3, 1)?;
    let encoder_output = Encoder::new(0., 16., 3, 0)?;

    // secret keys
    print!("generating RLWE key ...");
    let sk_rlwe = RLWESecretKey::new(&RLWE128_1024_1);
    println!(" DONE");
    print!("generating LWE key ...");
    let sk_in = LWESecretKey::new(&LWE128_630);
    println!(" DONE");
    print!("converting RLWE -> LWE key ...");
    let sk_out = sk_rlwe.to_lwe_secret_key();
    println!(" DONE");

    // bootstrapping key
    print!("generating BS key ...");
    let bsk = LWEBSK::new(&sk_in, &sk_rlwe, 5, 3);
    println!(" DONE");

    // messages
    let m: Vec<f64> = vec![-4., -3., -2., -1., 0., 1., 2., 3.];

    // in parallel, encode & encrypt
    let mut c: Vec<LWE> = vec![LWE::zero(sk_in.dimension)?; 8];
    m.par_iter().zip(c.par_iter_mut()).for_each(| (mi, ci) | {
        *ci = LWE::encode_encrypt(&sk_in, *mi, &encoder_input).expect("LWE::encode_encrypt failed.");
    });

    // in parallel, bootstrap
    let mut b: Vec<LWE> = vec![LWE::zero(sk_out.dimension)?; 8];
    // =========================================================================
    //  ISSUE:
    //
    //  bootstrapping does not work in parallel any more:
    //
    //      - with current Concrete/master(ae0bef, 19.1.2022), par_iter gives a compile error
    //      - with an old  Concrete/master(7dbb91,  9.7.2021), par_iter compiles & runs well
    //
    //  WISH:
    c.par_iter().zip(b.par_iter_mut()).for_each(| (ci, bi) | {
    //
    //  WORKS:
    //~ c.iter().zip(b.iter_mut()).for_each(| (ci, bi) | {
        *bi = ci.bootstrap_with_function(&bsk, |x| x*x, &encoder_output).expect("LWE::bootstrap_with_function failed.");
    });
    // =========================================================================

    // in parallel, decrypt & decode
    let mut d: Vec<f64> = vec![0.; 8];
    b.par_iter().zip(d.par_iter_mut()).for_each(| (bi, di) | {
        *di = bi.decrypt_decode(&sk_out).expect("LWE::decrypt_decode failed.");
    });

    println!("IN:  {:?}", m);
    println!("OUT: {:?}", d);

    Ok(())
}
