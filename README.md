
# Issue with parallelism


Bootstrapping does not work in parallel any more:

  - with current Concrete/master(ae0bef, 19.1.2022), par_iter gives a compile error
  - with an old  Concrete/master(7dbb91,  9.7.2021), par_iter compiles & runs well


## Compile errors:

```
error[E0277]: `RefCell<concrete_core::math::fft::polynomial::FourierPolynomial<concrete_fftw::array::AlignedVec<num_complex::Complex<f64>>>>` cannot be shared between threads safely
  --> src/main.rs:49:40
   |
49 |       c.par_iter().zip(b.par_iter_mut()).for_each(| (ci, bi) | {
   |  ________________________________________^^^^^^^^_-
   | |                                        |
   | |                                        `RefCell<concrete_core::math::fft::polynomial::FourierPolynomial<concrete_fftw::array::AlignedVec<num_complex::Complex<f64>>>>` cannot be shared between threads safely
50 | |     //
51 | |     //  WORKS:
52 | |     //~ c.iter().zip(b.iter_mut()).for_each(| (ci, bi) | {
53 | |         *bi = ci.bootstrap_with_function(&bsk, |x| x*x, &encoder_output).expect("LWE::bootstrap_with_function failed.");
54 | |     });
   | |_____- within this `[closure@src/main.rs:49:49: 54:6]`
   |
   = help: within `[closure@src/main.rs:49:49: 54:6]`, the trait `Sync` is not implemented for `RefCell<concrete_core::math::fft::polynomial::FourierPolynomial<concrete_fftw::array::AlignedVec<num_complex::Complex<f64>>>>`
   = note: required because it appears within the type `concrete_core::crypto::bootstrap::fourier::FourierBootstrapKey<concrete_fftw::array::AlignedVec<num_complex::Complex<f64>>, u64>`
   = note: required because it appears within the type `concrete::LWEBSK`
   = note: required because it appears within the type `&concrete::LWEBSK`
   = note: required because it appears within the type `[closure@src/main.rs:49:49: 54:6]`

error[E0277]: `RefCell<concrete_core::math::tensor::tensor::Tensor<concrete_fftw::array::AlignedVec<num_complex::Complex<f64>>>>` cannot be shared between threads safely
  --> src/main.rs:49:40
   |
49 |       c.par_iter().zip(b.par_iter_mut()).for_each(| (ci, bi) | {
   |  ________________________________________^^^^^^^^_-
   | |                                        |
   | |                                        `RefCell<concrete_core::math::tensor::tensor::Tensor<concrete_fftw::array::AlignedVec<num_complex::Complex<f64>>>>` cannot be shared between threads safely
50 | |     //
51 | |     //  WORKS:
52 | |     //~ c.iter().zip(b.iter_mut()).for_each(| (ci, bi) | {
53 | |         *bi = ci.bootstrap_with_function(&bsk, |x| x*x, &encoder_output).expect("LWE::bootstrap_with_function failed.");
54 | |     });
   | |_____- within this `[closure@src/main.rs:49:49: 54:6]`
   |
   = help: within `[closure@src/main.rs:49:49: 54:6]`, the trait `Sync` is not implemented for `RefCell<concrete_core::math::tensor::tensor::Tensor<concrete_fftw::array::AlignedVec<num_complex::Complex<f64>>>>`
   = note: required because it appears within the type `concrete_core::crypto::bootstrap::fourier::FourierBootstrapKey<concrete_fftw::array::AlignedVec<num_complex::Complex<f64>>, u64>`
   = note: required because it appears within the type `concrete::LWEBSK`
   = note: required because it appears within the type `&concrete::LWEBSK`
   = note: required because it appears within the type `[closure@src/main.rs:49:49: 54:6]`

error[E0277]: `RefCell<concrete_core::crypto::glwe::ciphertext::GlweCiphertext<Vec<u64>>>` cannot be shared between threads safely
  --> src/main.rs:49:40
   |
49 |       c.par_iter().zip(b.par_iter_mut()).for_each(| (ci, bi) | {
   |  ________________________________________^^^^^^^^_-
   | |                                        |
   | |                                        `RefCell<concrete_core::crypto::glwe::ciphertext::GlweCiphertext<Vec<u64>>>` cannot be shared between threads safely
50 | |     //
51 | |     //  WORKS:
52 | |     //~ c.iter().zip(b.iter_mut()).for_each(| (ci, bi) | {
53 | |         *bi = ci.bootstrap_with_function(&bsk, |x| x*x, &encoder_output).expect("LWE::bootstrap_with_function failed.");
54 | |     });
   | |_____- within this `[closure@src/main.rs:49:49: 54:6]`
   |
   = help: within `[closure@src/main.rs:49:49: 54:6]`, the trait `Sync` is not implemented for `RefCell<concrete_core::crypto::glwe::ciphertext::GlweCiphertext<Vec<u64>>>`
   = note: required because it appears within the type `concrete_core::crypto::bootstrap::fourier::FourierBootstrapKey<concrete_fftw::array::AlignedVec<num_complex::Complex<f64>>, u64>`
   = note: required because it appears within the type `concrete::LWEBSK`
   = note: required because it appears within the type `&concrete::LWEBSK`
   = note: required because it appears within the type `[closure@src/main.rs:49:49: 54:6]`

error: aborting due to 3 previous errors

For more information about this error, try `rustc --explain E0277`.
error: could not compile `rebase-parallel-issue`
```
